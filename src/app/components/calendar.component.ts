import { Component, OnInit } from "@angular/core";
import { CalendarService } from "../services/calendar.service";

@Component({
  selector: "calendar-app",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.css"]
})
export class CalendarComponent implements OnInit {
  years: number[] = null;
  monthes: string[] = null;
  weekDay: string[] = null;
  calendar: number[][] = null;
  currentDate: Date;
  pickYear: number;
  pickMonth: number;

  constructor(private calendarService: CalendarService) {}

  ngOnInit() {
    this.years = this.calendarService.getArrayOfYear();
    this.monthes = this.calendarService.getArrayOfMonth();
    this.currentDate = new Date();
    this.pickYear = this.currentDate.getFullYear();
    this.pickMonth = this.currentDate.getMonth();
    this.calendar = this.calendarService.getCalendar(
      this.pickYear,
      this.pickMonth
    );
    console.log(this.calendar);
    this.weekDay = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  }

  setYearData(_year: string) {
    this.pickYear = parseInt(_year);
    console.log(this.pickYear);
    this.currentDate.setFullYear(this.pickYear);
    this.calendar = this.calendarService.getCalendar(
      this.pickYear,
      this.pickMonth
    );
  }

  setMonthData(_month: string) {
    this.pickMonth = parseInt(_month);
    console.log(this.pickMonth);
    this.currentDate.setMonth(this.pickMonth);
     this.calendar = this.calendarService.getCalendar(
      this.pickYear,
      this.pickMonth
    );
  }

  createCalendar() {}
}
