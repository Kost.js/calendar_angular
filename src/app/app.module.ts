import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CalendarComponent } from './components/calendar.component';
import { CalendarService } from './services/calendar.service';

@NgModule({
  imports:      [ BrowserModule, FormsModule ],
  declarations: [ AppComponent, CalendarComponent ],
  bootstrap:    [ AppComponent ],
  providers: [ CalendarService]
})
export class AppModule { }
