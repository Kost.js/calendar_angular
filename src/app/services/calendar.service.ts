import { Injectable }from '@angular/core';
export class CalendarService {
  private years: number[] = [2010, 2011, 2012, 2013, 2014,2015, 2019, 2020];
  private CONST_DATA  = {
	      LENGTH_WEEK: 7,
  	    LENGTH_MONTH: 6,
  	    FIRST_MONTH_DATE: 1,
  	    CURRENT_DATE: new Date()
    }
  private First
  private monthes: string[] = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  constructor() { }
  getArrayOfYear(): number[] {
    return this.years.slice();
  }

  getArrayOfMonth(): string[] {
    return this.monthes.slice();
  }

  getCalendar(year: number, month: number): number[][] {
    let date = new Date(year, month);
    const currentMonth = date.getMonth();
    let isEqual = (this.CONST_DATA.CURRENT_DATE.getFullYear() === date.getFullYear()) && (this.CONST_DATA.CURRENT_DATE.getMonth() === date.getMonth());
    const currentDate = isEqual ? this.CONST_DATA.CURRENT_DATE.getDate() : 0;
      date.setDate(this.CONST_DATA.FIRST_MONTH_DATE);
      const dayOfWeekForFirstDate = date.getDay();
      const rawAmount = (this.CONST_DATA.LENGTH_WEEK - dayOfWeekForFirstDate) < 3 ? 6 : 5; 
      console.log(rawAmount);
      date.setDate(date.getDate() - dayOfWeekForFirstDate);

      let calendarArr = Array(this.CONST_DATA.LENGTH_WEEK*rawAmount).fill({
            _date: 0,
            _currentMonth: false,
            _currentDate: false
      });
  
      calendarArr = calendarArr.map( el => {
        let tempObj = (function(date, curMonth, curDate) {
          let el = {};
          el._date = date.getDate();
          el._currentMonth = (date.getMonth() === curMonth); 
          el._currentDate = (el._currentMonth && el._date === curDate);
          return el;
        })(date, currentMonth, currentDate);
        date.setDate(tempObj._date+1);
        return tempObj;
      });
      return calendarArr.reduce((rows, key, index) => (index % this.CONST_DATA.LENGTH_WEEK == 0 ? rows.push([key]) 
        : rows[rows.length-1].push(key)) && rows, []);
  }
}